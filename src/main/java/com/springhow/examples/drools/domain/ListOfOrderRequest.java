package com.springhow.examples.drools.domain;

import java.util.List;

public class ListOfOrderRequest {
    private String orderId;
    private List<OrderRequest> orderRequestItem;
    private Integer discount;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public List<OrderRequest> getOrderRequestItem() {
        return orderRequestItem;
    }

    public void setOrderRequestItem(List<OrderRequest> orderRequestItem) {
        this.orderRequestItem = orderRequestItem;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }
}
